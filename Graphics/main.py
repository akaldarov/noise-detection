import cv2, glob, os

except_frames = [58, 59, 84, 85, 208, 209, 282, 283, 308, 309, 432, 433, 506, 507, 532, 535, 533, 656, 657]
#Converting video to a bunch of frames
def VideoToFrames(path):
    videoCapture = cv2.VideoCapture()
    videoCapture.open(path) # Opening video
    frames = videoCapture.get(cv2.CAP_PROP_FRAME_COUNT) # Getting frames number
    for i in range(int(frames)):
          ret, frame = videoCapture.read()
          cv2.imwrite(f"output_frames/{i}.jpg", frame) # Creating images from frames
    return frames
 
#Calculating image hash          
def CalcImageHash(FileName):
    image = cv2.imread(FileName) #Reading image
    resized = cv2.resize(image, (8,8), interpolation = cv2.INTER_AREA) #Resizing image
    gray_image = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY) #Gray-scaling image
    avg = gray_image.mean() #Mean pixel color
    ret, threshold_image = cv2.threshold(gray_image, avg, 255, 0) #Binarization
    
    _hash=""
    for x in range(8):
        for y in range(8):
            val = threshold_image[x,y]
            if val==255:
                _hash = _hash + "1"
            else:
                _hash = _hash + "0"
    return _hash

#Comparing hashes of two images
def CompareHash(hash1, hash2):
    l = len(hash1)
    i = 0
    count = 0
    while i < l:
        if hash1[i] != hash2[i]:
            count+=1
        i+=1
    return count

#Analyzing hash numbers        
def CompareFrames(frames):
    hash1 = CalcImageHash("output_frames/0.jpg")
    for i in range(int(frames)):
        hash2 = CalcImageHash(f"output_frames/{i}.jpg")
        if CompareHash(hash1, hash2) > 1 or i in except_frames: #Comparing hashes
            os.remove(f'output_frames/{i}.jpg')

#Composing video from frames
def VideoComposing():
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('Videos/result.mp4', fourcc, 49, (1920, 1080))
    for filename in glob.glob('output_frames/*.jpg'):
        img = cv2.imread(filename)
        out.write(img)
    out.release()
 
if __name__ == '__main__':
     frames = VideoToFrames("Videos/output.mp4")
     CompareFrames(frames)
     VideoComposing()
